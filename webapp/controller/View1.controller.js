sap.ui.define(
	[
		"sap/ui/core/mvc/Controller",
		"sap/m/StandardListItem"
	],

	function (Controller, StandardListItem) {
		"use strict";

		return Controller.extend("ovly.logica.controller.View1", {
			onInit: function () {

			},
			onPress: function (oControlEvent) {
				//Ajudar na sugestão para a web ide
				//@type sap.m.Button
				var oSource = oControlEvent.getSource();

				var sButtonText = oSource.getText();
				// console.log(sButtonText);

				var vButtonType = oSource.getType();
				// console.log(vButtonType);
			},
			onChangeSwitch: function (oEvent) {

				// var bState = oEvent.getSource().getState();
				var bState = oEvent.getParameters().state;

				var oBotao = this.byId("btn_a");
				// var oBotao = this.getView().byId("btn_a"); // segunda opção para pegar pela view
				// oBotao.setEnabled(bState);
				oBotao.setVisible(bState);
			},
			onAdd: function () {
				//@type sap.m.List
				var oList = this.byId("list");
				var oInput = this.byId("name");
				var sValue = oInput.getValue();
				var oNewItem = new StandardListItem({
					title: sValue,
					icon: "sap-icon://activities"
				});

				// if (sValue !== "") {
				// 	oNewItem.setTitle(sValue);
				// 	oList.addItem(oNewItem);
				// }

				oList.addItem(oNewItem);
			},
			onDel: function () {
				//@type sap.m.List
				var oList = this.byId("list");
				oList.removeAllItems();
			}
		});
	});